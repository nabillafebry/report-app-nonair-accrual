# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
## copy WAR into image
#COPY target/report-air-accrual.jar /report-air-accrual.jar
## run application with this command line[
#CMD ["java", "-jar", "/report-air-accrual.jar"]
COPY target/*.jar app.jar

COPY optical-psyche-270017-2580909dacab.json service-account.json
ENV GOOGLE_APPLICATION_CREDENTIALS /service-account.json

ENTRYPOINT ["java","-jar","/app.jar"]
